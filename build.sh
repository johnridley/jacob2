#!/bin/bash

# bash build.sh <electronURL>

if [ ! -z "$1" ] 
then
	curl -J -L -o electron.zip $1
fi

if [ -f "electron.zip" -a ! -d "electron" ]
then
	unzip electron.zip -d electron
fi

if [ -d "electron" ]
then
	cp -R app electron/Electron.app/Contents/Resources/.
	cp electron.icns electron/Electron.app/Contents/Resources/electron.icns
	mv electron/Electron.app/Contents/MacOS/Electron electron/Electron.app/Contents/MacOS/JACOB
	cp yell.py electron/Electron.app/Contents/MacOS/JACOByell.py
	cp Info.plist electron/Electron.app/Contents/Info.plist
	rm -rf JACOB.app
	mv electron/Electron.app JACOB.app/
	rm -rf electron/
fi
