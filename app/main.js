const electron = require('electron');
const {app} = electron;
const {BrowserWindow} = electron;
const {Menu} = require('electron');

let win;

function createWindow() {
	win = new BrowserWindow({titleBarStyle: 'hidden-inset', width: 450, height: 670, resizable: false, fullscreen: false});
	win.loadURL(`file://${__dirname}/index.html`);

	win.on('closed', () => {
		win = null;
	});

	if (process.platform === 'darwin') 
	{
		const name = app.getName()
		const template = [{
			label: name,
			submenu: [
			{
				role: 'about'
			},
			{
				label: 'Debug',
				click ()
				{
					win.toggleDevTools();
				}
			},
			{
				type: 'separator'
			},
			{
				role: 'hide'
			},
			{
				role: 'hideothers'
			},
			{
				role: 'unhide'
			},
			{
				type: 'separator'
			},
			{
				role: 'quit'
			}
			]
		},
		{
			label: "Actions",
			submenu: [
			{
				label: 'Print with yell.py',
				accelerator: 'CmdOrCtrl+P',
				click ()
				{
					win.webContents.executeJavaScript('ui.print()');
				}
			},
			{
				label: 'Shut Down on Target',
				type: 'checkbox',
				accelerator: 'CmdOrCtrl+D',
				click (menuItem, browserWindow, event)
				{
					win.webContents.executeJavaScript('ui.setShutdown('+menuItem.checked+')');
				}
			}
			]
		}];
		const menu = Menu.buildFromTemplate(template);
		Menu.setApplicationMenu(menu);
	}

}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
		app.quit();
});

app.on('activate', () => {
	if (win === null) {
		createWindow();
	}
});
