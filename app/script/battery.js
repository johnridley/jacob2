var exec = require('child_process').exec;

function execute(command, callback)
{
	exec(command, function(error, stdout, stderr){ callback(stdout); });
};

function Battery()
{
	this.present = false;
	this.percentage = 0;
	this.current = 0;
	this.charging = false;
	this.pluggedin = false;
	this.timeEstimate = '0:00';

	this.cycles = 0;
	this.totalCycles = 0;
	this.fcc = 0;
	this.designcc = 0;
	this.condition = "";
	this.voltage = 0;
	this.serial = "";
	this.manufacturer = "";

	this.update = function()
	{
		execute('pmset -g batt', function(output) 
		{
			this.pluggedin = output.includes('AC');
			this.present = output.includes('Battery') && !(output.includes('removed'));
			this.charging = (output.includes('charging') || output.includes('finish')) && !(output.includes('not charging')) && !(output.includes('discharging'));
			this.percentage = output.match(/\d+(?=%)/)[0];
			var estimate = output.match(/\d+:\d+/);
			if(estimate == null || estimate[0] == "0:00")
				this.timeEstimate = "-:--";
			else
				this.timeEstimate = estimate[0];

		}.bind(this));
		execute('pmset -g rawbatt', function(output) 
		{
			this.fcc = output.match(/(?:FCC=)([0-9]+)/)[1];
			this.designcc = output.match(/(?:Design=)([0-9]+)/)[1];
			this.cycles = output.match(/(?:Cycles=)([0-9]+)\/([0-9]+)/)[1];
			this.totalCycles = output.match(/(?:Cycles=)([0-9]+)\/([0-9]+)/)[2];
			this.current = output.match(/-?\d+(?=mA)/)[0];
		}.bind(this));
		execute('system_profiler SPPowerDataType', function(output) 
		{
			this.condition = output.match(/Condition: (.+)/)[1];
			this.voltage = output.match(/\(mV\): (.+)/)[1];
			this.serial = output.match(/Serial Number: (.+)/)[1];
			this.manufacturer = output.match(/Manufacturer: (.+)/)[1];
		}.bind(this));
	}

	this.report = function()
	{
		var string = (new Date()) + '\n';
		string += 'Battery: ' + this.serial + '\n';
		string += 'Condition: ' + this.condition + '\n';
		string += 'Charge: ' + this.percentage + '% \n'; 
		string += 'Cycles: ' + Math.round(this.cycles / this.totalCycles * 100) + '% ' + this.cycles + "/" + this.totalCycles + '\n';
		string += 'Cap. Loss: ' + (100-Math.round(this.fcc / this.designcc * 100)) + '% ' + this.fcc + "/" + this.designcc + '\n';
		return string;
	}
} 
