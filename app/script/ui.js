const { remote } = require ('electron');

function BatteryUI()
{
	this.previousCharging = true;
	this.previousPluggedin = false;
	this.start = true;
	this.shutdown = false;
	this.logged = false;
	this.historyShown = false;

	this.discharge = false;
	this.timer = false;
	this.timerRunning = false;
	this.timerStart = null;
	this.timerStop = null;
	this.timerpercent = 0;
	this.stoppercent = 0;
	this.target = 0;

	this.yellpath = remote.process.execPath + "yell.py";

	this.database = new BatteryDatabase();
		
	$('#targetInput').change(function()
	{
		var value = Number($('#targetInput').val());
		if(value > 100) value = 100;
		if($('#targetInput').val() != '')
			$('#targetInput').val(value);

		$('#batteryTarget').animate({left: (value*229*0.01)+'px'});
		$('#timer').animate({top: '400px'});

		this.target = value;
		this.timer = true;
		this.timerStart = new Date();
		this.timerpercent = battery.percentage;
		this.timerRunning = true;
		if(this.target < battery.percentage)
			this.discharge = true;
		else
			this.discharge = false;

		$('#since').text('since ' + this.timerpercent + '% (' + this.target + '% target)');
		$('#button').text('stop');
		this.update();
	}.bind(this));

	$("#targetInput").bind({
		keydown: function(e) {
			if (e.shiftKey === true ) {
				if (e.which == 9) {
					return true;
				}
				return false;
			}
			if (e.which > 57) {
				return false;
			}
			if (e.which==32) {
				return false;
			}
			return true;
		}
	});

	$('#alertOverlay').click(function()
	{
		if(battery.present)
		{
			clearInterval(this.alert);
			$('#alertOverlay').fadeOut();
		}
	}.bind(this));

	$('#historyButton').click(function()
	{
		if(!this.historyShown)
		{
			this.batteryViewer.update();
			$('#historyOverlay').fadeIn();
			this.historyShown = true;
			$('#historyButton').css('background', '#f5f4f5');
		}
		else
		{
			$('#historyOverlay').fadeOut();
			this.historyShown = false;
			$('#historyButton').css('background', 'white');
			this.batteryViewer.reset();

		}
	}.bind(this));

	$('#button').click(function()
	{
		if(!this.timerRunning)
		{
			this.timer = false;
			$('#batteryTarget').animate({left: '0px'});
			$('#timer').animate({top: '700px'});
			$('#targetInput').val('');
		}
		else
		{
			this.timerRunning = false;
			this.timerStop = new Date();
			this.stoppercent = battery.percentage;
			$('#button').text('clear');
		}
		this.update();
	}.bind(this));

	this.update = function()
	{
		battery.update();
		setTimeout(function() 
		{
			$('body').fadeIn();
			if(!battery.present)
			{
				$('#alertOverlay').fadeIn();
				$('#alertOverlay').html("<br><br>Battery Not Present");
				return;
			}

			$('#percent').text(battery.percentage);
			var batteryWidth = (Number(battery.percentage) / 100) * 226;
			if(this.start)
			{
				$('#batteryFill').animate({width: batteryWidth+'px'});
				this.start = false;
			}
			else
				$('#batteryFill').css('width', batteryWidth);


			$('#condition').text(battery.condition);
			$('#batterySerial').text(" - " + battery.serial);
			$('#remaining').text(battery.timeEstimate);


			if(battery.pluggedin)
			{
				if(!this.previousPluggedin)
					this.updateStatus();
				$('#until').text('until full');
			}
			else
			{
				if(this.previousPluggedin)
					this.updateStatus();
				$('#until').text('until empty');
			}
			if(battery.charging)
			{
				if(!this.previousCharging)
					this.updateStatus();
			}
			else
			{
				if(this.previousCharging)
					this.updateStatus();
			}

			$('#cycleProgress').val(Math.round(battery.cycles / battery.totalCycles * 100));
			$('#cyclesPercent').text(Math.round(battery.cycles / battery.totalCycles * 100));
			$('#cycles').text(battery.cycles + "/" + battery.totalCycles);

			$('#capacityProgress').val(100-Math.round(battery.fcc / battery.designcc * 100));
			$('#capacityPercent').text(100-Math.round(battery.fcc / battery.designcc * 100));
			$('#capacity').text(battery.fcc + "/" + battery.designcc);

			var current = Number(battery.current);
			if(current >= 0)
			{
				$('#inCurrent').text(Math.abs(current) + " mA");
				$('#outCurrent').text('0 mA');
			}
			else
			{
				$('#outCurrent').text(Math.abs(current) + " mA");
				$('#inCurrent').text('0 mA');
			}

			$('#voltage').text((Number(battery.voltage)/1000) + " V");

			if(this.timerRunning)
			{
				if((this.discharge && battery.percentage <= this.target) || (!this.discharge && battery.percentage >= this.target))
				{
					this.timerRunning = false;
					this.timerStop = new Date();
					this.stoppercent = this.target;

					var diffMs = (this.timerStop - this.timerStart);
					var minutes = Math.round(((diffMs % 86400000) % 3600000) / 60000);
					var hours = Math.floor((diffMs % 86400000) / 3600000);
					if(minutes < 10)
						minutes = '0' + minutes;
					this.database.logTest(battery, new Date(), hours, minutes, this.timerpercent, String(this.target));
					this.alert = setInterval(function()
					{ 
						execute('osascript -e "set Volume 5"', function(output) {});
						execute('say ' + this.target + '%', function(){});
					}.bind(this), 4000);
					$('#alertOverlay').fadeIn();
					if(this.shutdown)
					{
						execute('osascript -e \'tell application "System Events" to shut down\'', function(output) {});
					}
					
					$('#button').text('clear');
				}
			}
			if(this.timerRunning)
				date = new Date();
			else
			{
				date = this.timerStop;
				$('#since').text('from ' + this.timerpercent + '% to ' + this.stoppercent + '%');
			}
			var diffMs = (date - this.timerStart);
			var minutes = Math.round(((diffMs % 86400000) % 3600000) / 60000);
			var hours = Math.floor((diffMs % 86400000) / 3600000);
			if(minutes < 10)
				minutes = '0' + minutes;
			$('#time').text(hours + ":" + minutes);

			if(!this.logged)
			{
				this.database.logBattery(battery);
				this.logged = true;
				this.batteryViewer = new BatteryViewer(this.database, battery);
				this.batteryViewer.update();
			}

		}.bind(this), 1000);
	}

	this.updateStatus = function()
	{
		this.previousCharging = battery.charging;
		this.previousPluggedin = battery.pluggedin;
		$('#indicator').animate({bottom: '-70px'}, function()
		{
			$('#indicator').text('Discharging');
			$('#indicator').css('background', '#3b99fc');
			if(battery.charging && battery.pluggedin)
			{
				$('#indicator').text('Connected: Charging');
				$('#indicator').css('background', 'orange');
			}
			if(!battery.charging && battery.pluggedin)
			{
				$('#indicator').text('Connected: Not Charging');
				$('#indicator').css('background', '#4ad15e');
			}
			$('#indicator').animate({bottom: '0px'});
		}.bind(this));
		
	}

	this.print = function()
	{
		var report = this.batteryViewer.getReport()
		execute('echo "' + report + '" | python ' + this.yellpath , function(output) {});
	}

	this.setShutdown = function(state)
	{
		this.shutdown = state;
	}
}

$(document).ready(function()
{
	var webFrame = require('electron').webFrame;
	webFrame.setVisualZoomLevelLimits(1, 1);
	webFrame.setLayoutZoomLevelLimits(0, 0);
	battery = new Battery();
	ui = new BatteryUI();
	ui.update();
	setInterval(ui.update.bind(ui), 5000);

});

