function BatteryDatabase()
{
	this.location = "~/Desktop/battery.txt"
	this.database = {};

	this.load = function(contents)
	{
		console.log(contents)
		try {
			this.database = JSON.parse(contents);
		} catch (error) {
			
		}	
	}
	execute("cat " + this.location, this.load.bind(this));

	this.save = function()
	{
		execute("echo '" + JSON.stringify(this.database, null, 2) + "' > " + this.location, function(output){})
	}

	this.get = function(serial)
	{
		return this.database[serial];
	}

	this.logBattery = function(battery)
	{
		if(!(battery.serial in this.database))
		{
			this.database[battery.serial] = 
			{
				"tests": []
			};
		}
		this.database[battery.serial]["cycles"] = battery.cycles;
		this.database[battery.serial]["totalCycles"] = battery.totalCycles;
		this.database[battery.serial]["fcc"] = battery.fcc;
		this.database[battery.serial]["designcc"] = battery.designcc;
		this.database[battery.serial]["condition"] = battery.condition;
		this.database[battery.serial]["manufacturer"] = battery.manufacturer;
		this.save();
	}

	this.logTest = function(battery, date, hours, minutes, startpercent, endpercent)
	{
		this.database[battery.serial]["tests"].unshift(
			{
				"date": date.toGMTString(),
				"hours": hours,
				"minutes": minutes,
				"startpercent": startpercent,
				"endpercent": endpercent
			}
		);
		this.save();
	}

	this.getKeys = function()
	{
		return Object.keys(this.database).reverse();
	}

	this.delete = function(key)
	{
		delete this.database[key]
		this.save();
	}
}

function BatteryViewer(database, battery)
{
	this.database = database;
	this.index = 0;

	$('#historyNext').click(function()
	{
		this.index++;
		if(this.index >= this.keys.length)
			this.index = 0;
		this.update();
	}.bind(this));

	$('#historyPrev').click(function()
	{
		this.index--;
		if(this.index < 0)
			this.index = this.keys.length - 1;
		this.update();
	}.bind(this));

	$('#historyDeleteButton').click(function()
	{
		this.database.delete(this.keys[this.index]);
		this.update();
	}.bind(this));

	this.update = function()
	{
		this.keys = this.database.getKeys();
		this.keys.sort(function(x,y){ return x == battery.serial ? -1 : y == battery.serial ? 1 : 0; });
		if(this.keys.length > 0)
		{
			var selected = this.database.get(this.keys[this.index]);
			$("#historySerial").text(this.keys[this.index]);
			$("#historyManufacturer").text(selected.manufacturer);
			$("#historyCondition").text(selected.condition);

			$('#historyCycleProgress').val(Math.round(selected.cycles / selected.totalCycles * 100));
			$('#historyCyclesPercent').text(Math.round(selected.cycles / selected.totalCycles * 100));
			$('#historyCycles').text(selected.cycles + "/" + selected.totalCycles);

			$('#historyCapacityProgress').val(100-Math.round(selected.fcc / selected.designcc * 100));
			$('#historyCapacityPercent').text(100-Math.round(selected.fcc / selected.designcc * 100));
			$('#historyCapacity').text(selected.fcc + "/" + selected.designcc);

			var testIndex = 0;
			var testHTML = "";
			selected.tests.forEach(test => 
			{
				testHTML += '<hr><div class="test">' + 
					test.date + "<br>" + test.hours + ":" + test.minutes + " " + 
					test.startpercent + "% &rightarrow; " + test.endpercent + "%</div>";
		
			});
			testHTML += "<hr>";

			$("#historyTests").html(testHTML);
		}
		else
		{
			$('#historyButton').click();
		}
	}

	this.reset = function()
	{
		this.index = 0;
	}

	this.getReport = function()
	{
		if(this.keys.length > 0)
		{
			var selected = this.database.get(this.keys[this.index]);
			var string = (new Date()).toGMTString() + '\n';
			string += 'Battery: ' + this.keys[this.index] + '\n';
			string += 'Manufacturer: ' + selected.manufacturer + '\n';
			string += 'Condition: ' + selected.condition + '\n';
			string += 'Cycles: ' + Math.round(selected.cycles / selected.totalCycles * 100) + '% ' + selected.cycles + "/" + selected.totalCycles + '\n';
			string += 'Cap. Loss: ' + (100-Math.round(selected.fcc / selected.designcc * 100)) + '% ' + selected.fcc + "/" + selected.designcc + '\n\n';
			
			selected.tests.forEach(test => 
			{
				string += test.date + "\n" + test.hours + ":" + test.minutes + " " + 
					test.startpercent + "% -> " + test.endpercent + "%\n";
		
			});
		}
		else
		{
			var string = "";
		}
		return string;
	}

}