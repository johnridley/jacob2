import subprocess, re, socket, argparse, os, sys

parser = argparse.ArgumentParser()
parser.add_argument("-s", help="create a server running command (%%s for substitution)", nargs='?', default="")
parser.add_argument('-p', help="port to use (default 1234)", nargs='?', const=1234, type=int, default=1234)
parser.add_argument('message', help="message to send", nargs='?', default="")

args = parser.parse_args()

networkconfig = subprocess.check_output(['ifconfig'])
pattern = re.compile(r'(?<=broadcast )\S*')
broadcast = pattern.search(networkconfig).group()

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

if args.s:
	sock.bind((broadcast, args.p))
	while True:
		data, addr = sock.recvfrom(2048)
		print data, addr
		os.system(args.s % (data)) 

else:
	if args.message == '':
		args.message = ''.join(sys.stdin.readlines())
	sock.sendto(args.message, (broadcast, args.p))

